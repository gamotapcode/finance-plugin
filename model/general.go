package model

type General struct {
	Id        string `pg:",pk,type:uuid, default:gen_random_uuid()"`
	CreatedAt int64
	CreatedBy string `pg:"type: uuid"`
	UpdatedAt int64
	UpdatedBy string `pg:"type: uuid"`
	IsDelete  bool   `pg:"default: false"`
}
