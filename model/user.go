package model

type User struct {
	// tableName is an optional field that specifies custom table name and alias.
	// By default go-pg generates table name and alias from struct name.
	tableName struct{} `pg:"user"` // default values are the same

	General
	Email    string
	Password string
	Type     string // 0 is free, 1 is premium
}

// var _ pg.BeforeInsertHook = (*User)(nil)

// func (b *User) BeforeInsert(ctx context.Context) (context.Context, error) {
// 	t := time.Now()
// 	b.CreatedAt = t.UnixMilli()
// 	b.UpdatedAt = t.UnixMilli()
// 	return ctx, nil
// }

// var _ pg.BeforeUpdateHook = (*User)(nil)

// func (b *User) BeforeUpdate(ctx context.Context) (context.Context, error) {
// 	t := time.Now()
// 	b.UpdatedAt = t.UnixMilli()
// 	return ctx, nil
// }
