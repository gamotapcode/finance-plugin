package model

import (
	"context"
	"time"

	"github.com/go-pg/pg/v10"
)

type Product struct {
	// tableName is an optional field that specifies custom table name and alias.
	// By default go-pg generates table name and alias from struct name.
	tableName struct{} `pg:"product"` // default values are the same

	General
	Name      string
	Code      string
	WareHouse string
}

var _ pg.BeforeInsertHook = (*Product)(nil)

func (b *Product) BeforeInsert(ctx context.Context) (context.Context, error) {
	t := time.Now()
	b.CreatedAt = t.UnixMilli()
	b.UpdatedAt = t.UnixMilli()
	return ctx, nil
}

var _ pg.BeforeUpdateHook = (*Product)(nil)

func (b *Product) BeforeUpdate(ctx context.Context) (context.Context, error) {
	t := time.Now()
	b.UpdatedAt = t.UnixMilli()
	return ctx, nil
}
