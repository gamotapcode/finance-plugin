package model

import (
	"context"
	"time"

	"github.com/go-pg/pg/v10"
)

type Bill struct {
	// tableName is an optional field that specifies custom table name and alias.
	// By default go-pg generates table name and alias from struct name.
	tableName struct{} `pg:"bill"` // default values are the same as struct field names

	General
	ProductId   string  `pg:"type:uuid"`
	UserId      string  `pg:"type:uuid"`
	Amount      int64   `pg:",notnull"`
	Price       float32 `pg:",notnull"`
	Currenrcy   string  `pg:"default:'VND'"`
	BuyAt       int64   `pg:",notnull"`
	Type        string  `pg:",notnull"`
	Description string
	Product     *Product `pg:"fk:product_id,rel:has-one"` // `fk:product_id` means that foreign key is product_id
	User        *User    `pg:"fk:user_id,rel:has-one"`    // `fk:user_id` means that foreign key is user_id

}

var _ pg.BeforeInsertHook = (*Bill)(nil)

func (b *Bill) BeforeInsert(ctx context.Context) (context.Context, error) {
	t := time.Now()
	b.CreatedAt = t.UnixMilli()
	b.UpdatedAt = t.UnixMilli()
	return ctx, nil
}

var _ pg.BeforeUpdateHook = (*Bill)(nil)

func (b *Bill) BeforeUpdate(ctx context.Context) (context.Context, error) {
	t := time.Now()
	b.UpdatedAt = t.UnixMilli()
	return ctx, nil
}
