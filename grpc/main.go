package main

import (
	"finance-plugin/connection"
	"finance-plugin/model"
	"finance-plugin/shared"

	"github.com/hashicorp/go-plugin"
)

// Here is a real implementation of KV that writes to a local file with
// the key name and the contents are the value of the key.
type Product struct{}
type Bill struct{}

func (b *Bill) CreateBills(productId string, userId string, amount int64, price float32, buyAt int64) error {
	bill := &model.Bill{
		ProductId: productId,
		UserId:    userId,
		Amount:    amount,
		Price:     price,
		BuyAt:     buyAt,
	}
	pg := connection.NewPostgres()
	_, err := pg.PgDB.Model(bill).Insert()
	return err
}
func (p *Product) CreateProducts(name string, code string) error {
	product := &model.Product{
		Name: name,
		Code: code,
	}
	pg := connection.NewPostgres()
	_, err := pg.PgDB.Model(product).Insert()
	return err
}
func (p *Product) UpdateProducts(string, string) error {
	return nil
}
func (p *Product) DeleteProducts(string, string) error {
	return nil
}
func (p *Product) GetProducts(string, string) error {
	return nil
}

func (p *Product) ListProducts(string, string) error {
	return nil
}

func main() {
	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: shared.Handshake,
		Plugins: map[string]plugin.Plugin{
			"kv": &shared.ProductGRPCPlugin{ProductImpl: &Product{}, BillImpl: &Bill{}},
		},

		// A non-nil value here enables gRPC serving for this plugin...
		GRPCServer: plugin.DefaultGRPCServer,
	})
}
