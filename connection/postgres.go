package connection

import (
	"context"
	"fmt"

	"finance-plugin/model"

	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
)

type Postgres struct {
	// config PostgresConfig
	// config *conf.Data
	PgDB *pg.DB
}

type dbLogger struct{}

func (d dbLogger) BeforeQuery(c context.Context, q *pg.QueryEvent) (context.Context, error) {
	return c, nil
}

func (d dbLogger) AfterQuery(c context.Context, q *pg.QueryEvent) (err error) {
	query, err := q.FormattedQuery()
	fmt.Println(q.StartTime)
	fmt.Println(query)
	fmt.Println("-----------------------------------------------------------------------")
	return err
}

// NewOrm connecr postgre
func (p *Postgres) NewOrm() *pg.DB {
	opt, err := pg.ParseURL("postgres://admin:123456@localhost:5432/stock?sslmode=disable")
	if err != nil {
		panic(err)
	}

	db := pg.Connect(opt)
	err = createSchema(db)
	if err != nil {
		panic(err)
	}
	db.AddQueryHook(dbLogger{})
	return db
}

// NewPostgres make a postgres connection
func NewPostgres() *Postgres {
	pg := &Postgres{}
	pg.PgDB = pg.NewOrm()
	return pg
}
func createSchema(db *pg.DB) error {
	models := []interface{}{
		(*model.Product)(nil),
		(*model.Bill)(nil),
	}

	for _, model := range models {
		err := db.Model(model).CreateTable(&orm.CreateTableOptions{
			IfNotExists: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
